import { Component } from '@angular/core';

/**
 * Generated class for the DemoDeviceComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'demo-device',
  templateUrl: 'demo-device.html'
})
export class DemoDeviceComponent {

  text: string;

  constructor() {
    console.log('Hello DemoDeviceComponent Component');
  }

}
