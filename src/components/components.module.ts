import { NgModule } from '@angular/core';
import { DemoDeviceComponent } from './demo-device/demo-device';
import { IonicModule } from "ionic-angular";

@NgModule({
	declarations: [DemoDeviceComponent],
	imports: [
    IonicModule
  ],
	exports: [DemoDeviceComponent]
})
export class ComponentsModule {}
