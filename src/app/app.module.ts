import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ItemDetailsPage } from '../pages/item-details/item-details';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ColorsPage } from "../pages/colors/colors";
import { ComponentsModule } from "../components/components.module";
import { ColorPickerModule } from "ngx-color-picker";

@NgModule({
  declarations: [
    MyApp,
    ItemDetailsPage,
    ListPage,
    ColorsPage
  ],
  imports: [
    BrowserModule,
    ComponentsModule,
    ColorPickerModule,
    IonicModule.forRoot(MyApp)
  ],
  exports: [
    ColorPickerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ItemDetailsPage,
    ListPage,
    ColorsPage

  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
