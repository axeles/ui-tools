export const PALETTE = {
  primary: {
    base: '#3880ff',
    shade: '#3171e0',
    tint: '#4c8dff',
    contrast: '#FFFFFF'
  },
  secondary: {
    base: '#0cd1e8',
    shade: '#0bb8cc',
    tint: '#24d6ea',
    contrast: '#FFFFFF'
  },
  tertiary: {
    base: '#7044ff',
    shade: '#633ce0',
    tint: '#7e57ff',
    contrast: '#FFFFFF'
  },
  success: {
    base: '#10dc60',
    shade: '#0ec254',
    tint: '#28e070',
    contrast: '#FFFFFF'
  },
  warning: {
    base: '#ffce00',
    shade: '#e0b500',
    tint: '#ffd31a',
    contrast: '#333'
  },
  danger: {
    base: '#f04141',
    shade: '#d33939',
    tint: '#f25454',
    contrast: '#FFFFFF'
  },
  dark: {
    base: '#222428',
    shade: '#1e2023',
    tint: '#383a3e',
    contrast: '#FFFFFF'
  },
  medium: {
    base: '#989aa2',
    shade: '#86888f',
    tint: '#a2a4ab',
    contrast: '#FFFFFF'
  },
  light: {
    base: '#f4f5f8',
    shade: '#d7d8da',
    tint: '#f5f6f9',
    contrast: '#333'
  }
};
