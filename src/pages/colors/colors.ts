import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PALETTE } from "./palette";
import * as _ from 'lodash';
import * as Color from 'color'

@Component({
  selector: 'page-colors',
  templateUrl: 'colors.html',
})
export class ColorsPage {
  paletteColors: {name: string, color: string}[];
  tints: string[];
  palette = _.cloneDeep(PALETTE);
  dividers = ['BASE COLORS', 'NOTIFICATIONS', 'GRAYSCALE'];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tints = Object.keys(this.palette[Object.keys(this.palette)[0]]);
    this.paletteColors = Object.keys(this.palette).map(el => {
      let contrastFontColor: string = 'black';
      this.tints.forEach(t => {
        const color = Color(this.palette[el][t]);
        contrastFontColor = color.isDark() ? 'white' : 'black'
      });
      return {name: el, color: contrastFontColor};
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ColorsPage');
  }

  onColorChange(colorName: string, tint: string) {
    const contrastColor = Color(this.palette[colorName][tint]);
    let index = this.paletteColors.findIndex(el => el.name === colorName);
    // console.log('+++[ColorsPage] index: ', index);
    if (tint === 'contrast') {
      this.paletteColors[index].color = contrastColor.isDark() ? 'white' : 'black'
    }
    if (tint === 'base') {
      // this.paletteColors[index].color = contrastColor.isDark() ? 'white' : 'black'
    }

  }
}
